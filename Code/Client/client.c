#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <stdbool.h>
#include <unistd.h>
#include "../Core_Modules/Communication_Protocol/communication.h"
#include "../Core_Modules/Client_Setup/client_setup.h"
#include "../Core_Modules/Hashing/hasher.h"
#include "../Core_Modules/File_Utils/file_utils.h"

//Globals ================================================================
char * operation;   //the operation to execute
char * filename;    //the name of the file
char * serverIp;    //the server to connect to
char * serverPort;
int client_sock;    //the client socket
char * message;     //message to send to/ receive from server
bool redirect;      //redirect request to server
//Functions ==============================================================
//Send a PUT request to the server
void requestPUT(){
    int offset;                 //offset of bytes. Used when writing to the message buffer
    int filenameBytes;          //how many bytes is the filename
    int fileBytes;              //how many bytes is the file
    int availableFileBytes;     //how many bytes remain to fit the file in the server message? We are using a maximum size for the server message
    char * fileBuffer;          //the buffer containing the file data
    FILE * fptr;                //the pointer to the opened file
    int bytesRead;              //how many bytes where read from a file
    unsigned long filenameHash; //the hash of the filename
    unsigned long fileHash;     //the hash of the file

    fptr = fopen(filename, "rb");
    if(!fptr){
        printf("[Client %d] Error - Cannot open requested file\n", getpid());
    }else{ //12:FILENAME:FILE
        //Construct client socket
        client_sock = setupClientSocket(serverIp, serverPort);
        if(client_sock == -1){
            printf("[Client %d] Error - Unable to connect to server\n", getpid());
        }else{
            //Is the file too large to send?
            filenameBytes = strlen(filename)+1;
            fseek(fptr, 0L, SEEK_END);
            fileBytes = ftell(fptr);
            availableFileBytes = MAX_SERVER_MESSAGE_SIZE - filenameBytes -1;
            if(fileBytes > availableFileBytes){
                printf("[Client %d] Error - file too large. Max server message size : %d\n", getpid(), MAX_SERVER_MESSAGE_SIZE);
            }else{ //The size of the file is OK
                //Build 12:FILENAME:FILE
                message = (char *) malloc(MAX_SERVER_MESSAGE_SIZE);
                memset(message, '\0', MAX_SERVER_MESSAGE_SIZE);
                message[0] = 12;
                offset = 1;
                memcpy(&message[offset], filename, filenameBytes);
                offset += filenameBytes;
                //Read file
                fileBuffer = (char *) malloc(fileBytes);
                memset(fileBuffer, '\0', fileBytes);
                bytesRead = 0;
                fseek(fptr, 0L, SEEK_SET);
                while(!feof(fptr)){
                    bytesRead += fread(&fileBuffer[bytesRead], sizeof(char), 1, fptr);
                }
                memcpy(&message[offset], fileBuffer, fileBytes);
                //send to server
                transmitMessage(message, (1 + filenameBytes + fileBytes), client_sock, COM_ERR_MESSAGES);
                shutdown(client_sock, SHUT_WR);
                //await for response
                memset(message, '\0', MAX_SERVER_MESSAGE_SIZE);
                receiveMessage(message, MAX_SERVER_MESSAGE_SIZE, client_sock, COM_ERR_MESSAGES);
                if(message[0] == 23){
                    offset = 1;
                    memcpy(&filenameHash, &message[offset], sizeof(unsigned long));
                    offset += sizeof(unsigned long);
                    memcpy(&fileHash, &message[offset], sizeof(unsigned long));
                    if( (filenameHash == djb2hash((unsigned char *)filename, filenameBytes)) && (fileHash == djb2hash((unsigned char *)fileBuffer, fileBytes) )){
                        printf("[Client %d] Server responded with P_OK. File integrity verified.\n", getpid());
                    }else{
                        printf("[Client %d] Server responded with P_OK. File hashes do not match.\n", getpid());
                    } 
                }else if(message[0] == 24){
                    printf("[Client %d] Server responded with P_ERROR. File upload unsuccesfull.\n", getpid());
                }else{
                    printf("[Client %d] Server responded with unrecognized response.\n", getpid());
                }
                //Realease Resources
                free(message);
                free(fileBuffer);
            }
        }
    }
    if(fptr){
        fclose(fptr);
    }
}
//Send a GET request to the server
void requestGET(){
    int offset;                 //offset of bytes. Used when writing to the message buffer
    int filenameBytes;          //how many bytes is the filename
    int fileBytes;              //how many bytes is the file
    int totalMessageBytes;      //total size of the message buffer
    unsigned long filenameHash; //the hash of the filename
    unsigned long fileHash;     //the hash of the file
    char * fileBuffer;          //the buffer containing the file data
    int operationStatus;        //the result after attempting to store the file retrieved
    int responseBytes;          //the bytes of the response

    //Construct client socket
    client_sock = setupClientSocket(serverIp, serverPort);
    if(client_sock == -1){
        printf("[Client %d] Error - Unable to connect to server\n", getpid());
    }else{
        //Build 13:FILENAME
        filenameBytes = strlen(filename) + 1;
        totalMessageBytes = filenameBytes + 1;
        message = (char *) malloc(totalMessageBytes);
        memset(message, '\0', totalMessageBytes);
        message[0] = 13;
        memcpy(&message[1], filename, filenameBytes);
        //send to server
        transmitMessage(message, totalMessageBytes, client_sock, COM_ERR_MESSAGES);
        shutdown(client_sock, SHUT_WR);
        //receive response
        message = (char *) realloc(message, MAX_SERVER_MESSAGE_SIZE);
        memset(message, '\0', MAX_SERVER_MESSAGE_SIZE);
        responseBytes = receiveMessage(message, MAX_SERVER_MESSAGE_SIZE, client_sock, COM_ERR_MESSAGES);
        if(message[0] == 28){   //G_UNKNOWN
            printf("[Client %d] Server responded with \"File Not Found\".\n", getpid());
        }else if(message[0] == 27){ //G_ERROR
            printf("[Client %d] Server responded with \"Error In Getting The File\".\n", getpid());
        }else if(message[0] == 25){   //G_OK is 25:hash(filename):hash(file):FILE
            offset = 1;
            memcpy(&filenameHash, &message[offset], sizeof(unsigned long));
            offset += sizeof(unsigned long);
            memcpy(&fileHash, &message[offset], sizeof(unsigned long));
            offset += sizeof(unsigned long);
            fileBytes = responseBytes - offset;
            fileBuffer = (char *) malloc(fileBytes);
            memcpy(fileBuffer, &message[offset], fileBytes);
            if( (filenameHash == djb2hash((unsigned char *)filename, filenameBytes)) && (fileHash == djb2hash((unsigned char *)fileBuffer, fileBytes)) ){
                printf("[Client %d] Server response is G_OK. File integrity verified.\n", getpid());
            }else{
                printf("[Client %d] Server response is G_OK. File hashes do not match.\n", getpid());
            }
            operationStatus = storeFile(filename, NULL, fileBuffer, fileBytes);
            if(operationStatus == -1){
                printf("[Client %d] Error in storing file.\n", getpid());
            }
            free(fileBuffer);
        }else if(message[0] == 26){    //REDIRECT 26:IP:PORT
            offset = 1;
            memcpy(serverIp, &message[offset], INET_ADDRSTRLEN);
            offset += INET_ADDRSTRLEN;
            memcpy(serverPort, &message[offset], MAX_PORT_SIZE);
            redirect = true;
        }else{
            printf("[Client %d] Unknown response\n", getpid());
        }
    }
    //Release Resources
    free(message);
}
//Send a DELETE request to the server
void requestDELETE(){
    int filenameBytes;          //how many bytes is the filename
    int totalMessageBytes;      //total size of the message buffer

    client_sock = setupClientSocket(serverIp, serverPort);
    if(client_sock == -1){
        printf("[Client %d] Error - Unable to connect to server\n", getpid());
    }else{
        //Build 14:FILENAME
        filenameBytes = strlen(filename) + 1;
        totalMessageBytes = filenameBytes + 1;
        message = (char *) malloc(totalMessageBytes);
        memset(message, '\0', totalMessageBytes);
        message[0] = 14;
        memcpy(&message[1], filename, filenameBytes);
        //send to server
        transmitMessage(message, totalMessageBytes, client_sock, COM_ERR_MESSAGES);
        shutdown(client_sock, SHUT_WR);
        //receive response
        message = realloc(message, MAX_SERVER_MESSAGE_SIZE);
        memset(message, '\0', MAX_SERVER_MESSAGE_SIZE);
        receiveMessage(message, MAX_SERVER_MESSAGE_SIZE, client_sock, COM_ERR_MESSAGES);
        if(message[0] == 29){   //D_OK
            printf("[Client %d] Server responded with \"Delete OK\".\n", getpid());
        }else if(message[0] == 30){ //D_ERROR
            printf("[Client %d] Server responded with \"Error In Getting The File\".\n", getpid());
        }
    }
    //Release Resources
    free(message);
}

int main(int argc, char * argv[]){
    //Argument Validation
    if(argc < 5){
        printf("[Client %d] Error - Insufficient Arguments\n", getpid());
        exit(1);
    }
    operation = argv[1];
    filename = argv[2];
    serverIp = argv[3];
    serverPort = argv[4];
    if( (strcmp(operation, "get") != 0) && (strcmp(operation, "put") != 0) && (strcmp(operation, "delete") != 0) ){
        printf("[Client %d] Error - Invalid operation requested\n", getpid());
        exit(1);
    }
    if( (atoi(serverIp) < 0) || (atoi(serverPort) < 0) ){
        printf("[Client %d] Error - Invalid serverIp and serverPort\n", getpid());
        exit(1);
    }
    
    //request operation
    if(strcmp(operation, "put") == 0){
        requestPUT();
    }else if(strcmp(operation, "get") == 0){
        redirect = false;
        requestGET();
        if(redirect){   //if you got a REDIRECT response, start a new connection with the updated data
            close(client_sock);
            requestGET();
        }
    }else{
        requestDELETE();
    }
    close(client_sock);
    return 0;
}