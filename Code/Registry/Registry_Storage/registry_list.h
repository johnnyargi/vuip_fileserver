#ifndef REG_LIST
#define REG_LIST
/*
This file will provide an interface for storing and retrieving server
info to/from the registry. 
The implementation should protect from race conditions during multithreaded access
*/
#include <arpa/inet.h>
#include "../../Core_Modules/Communication_Protocol/communication.h"

//Structures ==================================================================
//This will be a list of server nodes maintained by the registry for mapping
typedef struct server_node_t{
    int socket_fd;                  //This will be used when a connection remains open. Otherwise set to -1.
    char ip[INET_ADDRSTRLEN];       //The ip of the server. Kept as string to easily read by getaddrinfo.
    char port[MAX_PORT_SIZE];       //The port of the server. Kept as string to easily read by getaddrinfo.
    int id;                         //The id of the server
    struct server_node_t * next;
}Server_Node;

//Functionality =============================================================
//Initialize parameters
void initializeRegistry();
//Retrieve this server's information. Returns -1 if not found
Server_Node * findServer(int id);
//Add a server to the list and return its id. Store the socket descriptor if the connection will be kept alive or set to -1. Returns -1 on error
int addServer(char * ip, char * port, int socket, int maxServers);
//Broadcast start message to list and close their sockets afterwards
void broadcastStart();
//Memory Cleanup ===========================================================
void cleanupRegistry();
//Debugging Utilitues ==================================
void printRegistry();
#endif