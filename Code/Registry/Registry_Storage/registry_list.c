/*
This file implements the respective .h interface
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
#include <pthread.h>
#include "../../Core_Modules/Communication_Protocol/communication.h"
#include "registry_list.h"

//Globals =============================
Server_Node * registryHead;      //the head of the linked list of servers
Server_Node * registryTail;      //the tail of the linked list of servers
int servers;                        //number of servers so far
pthread_mutex_t writerLock;             //mutex for the writers
pthread_mutexattr_t writerLockAttr; 

//Functionality =============================================================
//Initialize parameters
void initializeRegistry(){
    registryHead = NULL;
    registryTail = NULL;
    servers = 0;
    pthread_mutexattr_init(&writerLockAttr);
    pthread_mutex_init(&writerLock, &writerLockAttr);
}
//Retrieve this server's port. Returns NULL if not found.
Server_Node * findServer(int id){
    Server_Node * currNode;
    //the server's variable is written by the servers and might be liable to a race condition
    pthread_mutex_lock(&writerLock);
    if((id > (servers-1)) || (id < 0) ){
        pthread_mutex_unlock(&writerLock);
        return NULL;
    }
    pthread_mutex_unlock(&writerLock);
    for(currNode = registryHead; currNode != NULL; currNode = currNode->next){
        if(currNode->id == id){
            break;
        }
    }
    return currNode;
}
//Add a server to the list and return its id. Store the socket descriptor if the connection will be kept alive or set to -1. Returns -1 on error
int addServer(char * ip, char * port, int socket, int maxServers){
    Server_Node * currNode;

    pthread_mutex_lock(&writerLock);
    if(servers < maxServers){
        currNode = (Server_Node *) malloc(sizeof(Server_Node));
        currNode->socket_fd = socket;
        memcpy(currNode->ip, ip, INET_ADDRSTRLEN);
        memcpy(currNode->port, port, MAX_PORT_SIZE);
        currNode->id = servers;
        servers++;
        currNode->next = NULL;
        if(registryHead == NULL){
            registryHead = currNode;
            registryTail = registryHead;
        }else {
            registryTail->next = currNode;
            registryTail = currNode;
        }
        pthread_mutex_unlock(&writerLock);
        return currNode->id;
    }
    pthread_mutex_unlock(&writerLock);
    return -1;
}
//Broadcast start message to list and close their sockets afterwards
void broadcastStart(){
    Server_Node * currNode;
    char message[MAX_REGISTRY_MESSAGE_SIZE];
    
    memset(message, '\0', MAX_REGISTRY_MESSAGE_SIZE);
    message[0] = 40;
    message[1] = '\0';
    for(currNode = registryHead; currNode != NULL; currNode = currNode->next){
        transmitMessage(message, MAX_REGISTRY_MESSAGE_SIZE, currNode->socket_fd, COM_ERR_MESSAGES);
        shutdown(currNode->socket_fd, SHUT_RDWR);
        close(currNode->socket_fd);
        currNode->socket_fd = -1;
    }
}
//Memory Cleanup ===========================================================
void cleanupRegistry(){
    Server_Node * prevEntry;     //previously examined entry
    Server_Node * currEntry;     //current entry
    prevEntry = currEntry = registryHead;
    while(currEntry != NULL){
        currEntry = currEntry->next;
        free(prevEntry);
        prevEntry = currEntry;
    }
    registryHead = NULL;
    pthread_mutex_destroy(&writerLock);
}
//Debugging Utilitues ==================================
void printRegistry(){
    Server_Node * currEntry;     //current entry
    currEntry = registryHead;
    while(currEntry != NULL){
        printf("Server : %s\n", currEntry->ip);
        printf("    Port : %s\n", currEntry->port);
        printf("    Id : %d\n", currEntry->id);
        currEntry = currEntry->next;
    }
}