/*
This will be the registy server of our implementation
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <sys/socket.h>
#include <signal.h>
#include <unistd.h>
#include "Registry_Storage/registry_list.h"
#include "../Core_Modules/Server_Setup/server_setup.h"
#include "../Core_Modules/Communication_Protocol/communication.h"

//Globals ========================================================================================
char * maxServersStr;           //The maximum number of servers that the registry contains
int maxServers;                 //Same as above but as an integer    
bool started_servers = false;   //Did a broadcast of START to the servers occur?

//Functions =========================================================================================
//Handle termination by notifying the client
void terminationHandler(int sig){
    if(sig == SIGINT){
        shutdownServer();
        cleanupRegistry();
        exit(0);
    }
}
//This will be run by the threads of the server right after succesfully accepting a connection
void requestProcessor(int client_sock, char * ipAddress){
    char receivedMessage[MAX_REGISTRY_MESSAGE_SIZE];    //the buffer of the received message
    char sentMessage[MAX_REGISTRY_MESSAGE_SIZE];        //the buffer of the message to send
    char port[MAX_PORT_SIZE];                           //the port of a server
    int id;                                             //the id of a server
    int offset;                                         //byte offset to be used when constructing reponse message
    Server_Node * requestedServer;                      //the server returned by a "get server" type of request

    memset(receivedMessage, '\0', MAX_REGISTRY_MESSAGE_SIZE);
    memset(sentMessage, '\0', MAX_REGISTRY_MESSAGE_SIZE);
    //Read message
    receiveMessage(receivedMessage, MAX_REGISTRY_MESSAGE_SIZE, client_sock, COM_ERR_MESSAGES);
    //Select operation depending on message
    if((receivedMessage[0] == 10)){     //Registration (Only when the list is not full) - Respond with 20:N:ID
        memcpy(port, &receivedMessage[1], MAX_PORT_SIZE);
        id = addServer(ipAddress, port, client_sock, maxServers);
        if(id >= 0){
            sentMessage[0] = 20;
            offset = 1;
            memcpy(&sentMessage[offset], maxServersStr, strlen(maxServersStr)+1);
            offset += strlen(maxServersStr)+1;
            memcpy(&sentMessage[offset], &id, sizeof(int));
            transmitMessage(sentMessage, MAX_REGISTRY_MESSAGE_SIZE, client_sock, COM_ERR_MESSAGES);
            if(id == (maxServers - 1)){     //Broadcast START (40) - this will also close the sockets kept open so far
                printf("[Registry] Broadcasting START\n");
                broadcastStart();
            }
        }else{
            shutdown(client_sock, SHUT_RDWR);
            close(client_sock);
        }
    }else if(receivedMessage[0] == 11){   //Find server
        memcpy(&id, &receivedMessage[1], sizeof(int));
        requestedServer = findServer(id); 
        if(requestedServer == NULL){   //server wasn't found - respond with Q_ERROR (22)
            sentMessage[0] = 22;
            sentMessage[1] = '\0';
        }else{  //server was found, send its ip and port 21:IP:PORT
            sentMessage[0] = 21;
            offset = 1;            
            memcpy(&sentMessage[offset], requestedServer->ip, INET_ADDRSTRLEN);
            offset += INET_ADDRSTRLEN;
            memcpy(&sentMessage[offset], requestedServer->port, MAX_PORT_SIZE);
        }
        transmitMessage(sentMessage, MAX_REGISTRY_MESSAGE_SIZE, client_sock, COM_ERR_MESSAGES);
        shutdown(client_sock, SHUT_RDWR);
        close(client_sock);
    }else{ //we don't want to close the socket if START has not been broadcast, that' why we include this "else" case
        shutdown(client_sock, SHUT_RDWR);
        close(client_sock);
    }
}

int main(int argc, char * argv[]){
    int port;

    //Argument checker
    if(argc < 3){
        printf("[Error] Insufficient command line arguments.\n");
        exit(1);
    }
    port = atoi(argv[1]);
    maxServersStr = argv[2];
    maxServers = atoi(maxServersStr);
    if((port < 0) || (maxServers <0)){
        printf("[Error] Invalid command line arguments.\n");
        exit(1);
    }
    //Initialize the registry
    initializeRegistry();
    //Start server and handle requests  
    setupServer(port, requestProcessor);
    //Memory cleanup
    cleanupRegistry();
    return 0;
}