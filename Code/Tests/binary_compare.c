#include <stdio.h>
#include <stdlib.h>
#include "../Core_Modules/File_Utils/file_utils.h"
#include "../Core_Modules/Hashing/hasher.h"

int main(int argc, char * argv[]){
    int bytesToRead1, bytesToRead2;
    char * binary1;
    char * binary2;
    int i;
    unsigned long hashFile1, hashFile2;

    if(argc < 3){
        printf("Insufficient arguments\n");
        return -1;
    }


    bytesToRead1 = getFileBytes(argv[1], NULL);
    binary1 = (char *) malloc(sizeof(char)*bytesToRead1);
    readFile(argv[1], NULL, binary1, bytesToRead1);
    printf("Read file 1 returns : %d bytes\n", bytesToRead1);

    bytesToRead2 = getFileBytes(argv[2], NULL);
    binary2 = (char *) malloc(sizeof(char)*bytesToRead2);
    readFile(argv[2], NULL, binary2, bytesToRead2);
    printf("Read file 2 returns : %d bytes\n", bytesToRead2);

    if(bytesToRead1 != bytesToRead2){
        printf("Different sizes.\n");
        free(binary1);
        free(binary2);
        return 0;
    }

    for(i=0; i<bytesToRead1; i++){
        if(binary1[i] != binary2[i]){
            printf("Binaries do not have the same content. Stopped at byte : %d\n", i);
            free(binary1);
            free(binary2);
            return 0;
        }
    }

    hashFile1 = djb2hash((unsigned char *) binary1, bytesToRead1);
    hashFile2 = djb2hash((unsigned char *) binary2, bytesToRead2);

    printf("File 1 hash : %ld , file 2 hash : %ld \n", hashFile1, hashFile2);

    if(hashFile1 != hashFile2){
        printf("Different hashes.\n");
        free(binary1);
        free(binary2);
        return 0;
    }
    printf("Files are the same.\n");
    free(binary1);
    free(binary2);
    return 0;
}