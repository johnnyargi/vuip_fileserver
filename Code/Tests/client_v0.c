#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netinet/in.h>
#include <netdb.h>

int sock_fd;            //socket file descriptor
//data for the getaddrinfo *******************
struct addrinfo hints;
struct addrinfo *servinfo, *addrPtr;
int addrResult;
int connectionStatus;

//Setup client socket
void setupClientSocket(char * argv[]){
    //Get the server address and the port from the command line ==========================
    //And search for possible network connections
    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    addrResult = getaddrinfo(argv[1], argv[2], &hints, &servinfo);
    if(addrResult != 0){
        fprintf(stderr, "[Error] Get address info error : %s\n", gai_strerror(addrResult));
    }
    //Connect to one of the given results ===================================================
    for(addrPtr = servinfo; addrPtr != NULL; addrPtr = addrPtr->ai_next){
        sock_fd = socket(AF_INET, SOCK_STREAM, 0);
        if(sock_fd == -1){
            perror("[Error] Cannot create socket\n");
            continue;
        }
        connectionStatus = connect(sock_fd, addrPtr->ai_addr, addrPtr->ai_addrlen);
        if(connectionStatus == -1){
            perror("[Error] Cannot connect\n");
            close(sock_fd);
            continue;
        }
        break;  //succesfull connection
    }
    if (addrPtr == NULL) { //exited loop without connection
        fprintf(stderr, "failed to connect\n");
        exit(2);
    }
}

int main(int argc, char * argv[]){
    setupClientSocket(argv);
    
    close(sock_fd);
    freeaddrinfo(servinfo);
    return 0;
}