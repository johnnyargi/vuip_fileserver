#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netinet/in.h>

int main(int argc, char * argv[]){
    //create a socket
    int sock_fd;
    sock_fd = socket(AF_INET, SOCK_STREAM, 0);
    //specify address to connect to
    struct sockaddr_in server_address;
    server_address.sin_family = AF_INET;
    server_address.sin_port = htons(3000);
    server_address.sin_addr.s_addr = INADDR_ANY;
    //now connect
    int connection_status = connect(sock_fd, (struct sockaddr *) &server_address, sizeof server_address); 
    if(connection_status){
        perror("[Client] Connection could not be established\n");
    }
    //receive response
    char server_response[64];
    recv(sock_fd, server_response, sizeof server_response, 0);
    send(sock_fd, "Hello back!", 12, 0);
    //print response
    printf("[Client] Got : %s", server_response);
    //close the socket
    close(sock_fd);
    return 0;
}