#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netinet/in.h>

int main(int argc, char * argv[]){
    char server_message[] = "Hello from server!\n";
    //create socket
    int sock_fd;
    sock_fd = socket(AF_INET, SOCK_STREAM, 0);
    //specify address
    struct sockaddr_in server_address;
    server_address.sin_family = AF_INET;
    server_address.sin_port = htons(3000);
    server_address.sin_addr.s_addr = INADDR_ANY;  
    //bind to address
    bind(sock_fd, (struct sockaddr *) &server_address, sizeof server_address);
    //listen for connections
    listen(sock_fd, 5);
    //accept a connection
    int client_sock;
    client_sock = accept(sock_fd, NULL, NULL);
    //send data
    send(client_sock, server_message, sizeof(server_message), 0);
    shutdown(client_sock, SHUT_WR);
    memset(server_message , '\0', sizeof(server_message));
    recv(client_sock, server_message, sizeof(server_message), 0);
    printf("Got : %s\n", server_message);
    //close
    close(sock_fd);
    return 0;
}