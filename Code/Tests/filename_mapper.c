#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../Core_Modules/File_Utils/file_utils.h"

int main(int argc, char * argv[]){
    if(argc < 3){
        printf("Insufficient arguments\n");
        return -1;
    }
    printf("Filename \"%s\" maps to server : %d\n", argv[1], fileMapper(argv[1], atoi(argv[2])));
    return 0;
}