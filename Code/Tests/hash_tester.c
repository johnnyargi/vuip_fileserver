#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../Core_Modules/File_Utils/file_utils.h"
#include "../Core_Modules/Hashing/hasher.h"

int main(int argc, char * argv[]){
    int bytesToRead;
    char * fileBuffer;

    if(argc < 2){
        printf("Insufficient arguments\n");
        return -1;
    }


    bytesToRead = getFileBytes(argv[1], NULL);
    printf("Get file bytes by seek, returns : %d bytes\n", bytesToRead);
    fileBuffer = (char *) malloc(sizeof(char)*bytesToRead);
    bytesToRead = readFile(argv[1], NULL, fileBuffer, bytesToRead);
    printf("Read file returns : %d bytes\n", bytesToRead);

    printf("File \"%s\" hashes to : %ld\n", argv[1], djb2hash((unsigned char *) fileBuffer, bytesToRead));

    free(fileBuffer);
    return 0;
}