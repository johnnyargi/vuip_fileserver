#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>
#include "../Core_Modules/Communication_Protocol/communication.h"
#include "../Core_Modules/Client_Setup/client_setup.h"
#include "../Core_Modules/Server_Setup/server_setup.h"
#include "Operations/operations.h"
//Globals ==============================================================================================
/*
The following variables will be written before spawning threads
They will only be read by the server's threads, so no race conditions will happen
*/
RegistryInfo * regInfo;   //it will hold the data relevant to the registry
ServerInfo * serverInfo;    //it will hold the data relevant to this server
//Functions ============================================================================================
//This function will register this server to the registry and wait until the START message is received
void registerServer(){
    char message[MAX_REGISTRY_MESSAGE_SIZE];    //the message buffer to be sent/retrieved over the network
    bool registered = false;                    //is the server registered?
    int offset;                                 //used to read from message buffer 
    int client_sock;                            //The client's socket descriptor

    //Send register messsage to registry
    memset(message, '\0', MAX_REGISTRY_MESSAGE_SIZE);
    message[0] = 10;
    memcpy(&message[1], serverInfo->serverPort, strlen(serverInfo->serverPort));
    message[strlen(serverInfo->serverPort)+1] = '\0';
    client_sock = setupClientSocket(regInfo->registryIp, regInfo->registryPort);
    if(client_sock < 0){
        printf("[Server %d][Error] Cannot connect to registry.\n", getpid());
        exit(1);
    }
    transmitMessage(message, MAX_REGISTRY_MESSAGE_SIZE, client_sock, COM_ERR_MESSAGES);
    //Wait to receive START message from registry - ignore other messages
    while(!registered){
        memset(message, '\0', MAX_REGISTRY_MESSAGE_SIZE);
        receiveMessage(message, MAX_REGISTRY_MESSAGE_SIZE, client_sock, COM_ERR_MESSAGES);
        if(message[0] == 20){
            offset = 1;
            regInfo->servers = atoi(&message[offset]);
            offset += strlen(&message[offset])+1;
            serverInfo->serverId = (int) message[offset];
            if((regInfo->servers < 0) || (serverInfo->serverId < 0) || (serverInfo->serverId > (regInfo->servers-1)) ){
                printf("[Server %d][Error] Registry responded with invalid data. Shuting down.\n", getpid());
                exit(1);
            }
        }else if(message[0] == 40){
            printf("[Server %d] Received START\n", getpid());
            registered = true;
        }
    }
    close(client_sock);
}
//This function will be run the the server's threads
void requestProcessor(int client_sock, char  * ipAddress){
    char * message;         //the whole message received from the client
    int messageBytes;       //total bytes of message
    
    //Read the message from the client
    message = (char *) malloc(MAX_SERVER_MESSAGE_SIZE);
    memset(message, '\0', MAX_SERVER_MESSAGE_SIZE);
    messageBytes = receiveMessage(message, MAX_SERVER_MESSAGE_SIZE, client_sock, COM_ERR_MESSAGES);
    //Process Request   
    if(message[0] == 12){           //PUT 12:FILENAME:FILE
        processPUT(regInfo, serverInfo, client_sock, message, messageBytes);
    }else if(message[0] == 13){     //GET 13:FILENAME
        processGET(regInfo, serverInfo, client_sock, message, messageBytes);
    }else if(message[0] == 14){     //DELETE 14:FILENAME
        processDELETE(regInfo, serverInfo, client_sock, message, messageBytes);
    }else{
        shutdown(client_sock, SHUT_RDWR);
        close(client_sock);
    }
    //Release Memory Resources
    free(message);
}
//Main
int main(int argc, char * argv[]){
    int serv_port;

    //Get and validate command line arguments
    if(argc < 5){
        printf("[Server %d][Error] Insufficient arguments.\n", getpid());
        exit(1);
    }

    serverInfo = (ServerInfo *) malloc(sizeof(ServerInfo));
    serverInfo->serverPort = argv[1];
    serverInfo->directory = argv[4];
    serv_port = atoi(serverInfo->serverPort); 

    regInfo = (RegistryInfo *) malloc(sizeof(RegistryInfo));
    regInfo->registryIp = argv[2];
    regInfo->registryPort = argv[3];
    
    if(( serv_port < 0 ) || (atoi(regInfo->registryPort) < 0)){
        printf("[Server %d][Error] Invalid argument list.\n", getpid());
        exit(1);
    }
    //Register the server
    registerServer();
    //Start the server
    setupServer(serv_port, requestProcessor);

    return 0;
}