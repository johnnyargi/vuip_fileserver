#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <unistd.h>
#include "../../Core_Modules/Client_Setup/client_setup.h"
#include "../../Core_Modules/Hashing/hasher.h"
#include "../../Core_Modules/File_Utils/file_utils.h"
#include "operations.h"

//For debugging only ****
//Assume that it's already checked that they have the same bytes
void binaryCompare(char * binary1, char * binary2, int bytesToRead){
    int i;
    unsigned long hashFile1, hashFile2;
    bool fail;

    fail = false;
    for(i=0; i<bytesToRead; i++){
        if(binary1[i] != binary2[i]){
            printf("Binaries do not have the same content. Stopped at byte : %d\n", i);
            fail = true;
            break;
        }
    }

    if(!fail){
        hashFile1 = djb2hash((unsigned char *) binary1, bytesToRead);
        hashFile2 = djb2hash((unsigned char *) binary2, bytesToRead);
        printf("File 1 hash : %ld , file 2 hash : %ld \n", hashFile1, hashFile2);
        if(hashFile1 != hashFile2){
            printf("Different hashes.\n");
        }else{
            printf("Files are the same.\n");
        }
    }
}
//Fills a FileInfo structure with data derived from a message. Operation can be 'p', 'g' or 'd' , depending on PUT, GET, DELETE respectively
void fillFileInformation(char operation, FileInfo * fileInfo, ServerInfo * serverInfo, char * message, int messageBytes){
    //Read the filename
    fileInfo->filenameBytes = strlen(&message[1])+1;
    fileInfo->filename = (char *) malloc(fileInfo->filenameBytes);
    memset(fileInfo->filename, '\0', fileInfo->filenameBytes);
    strcpy(fileInfo->filename, &message[1]);
    if(operation == 'p'){
        fileInfo->fileBytes = messageBytes - fileInfo->filenameBytes - 1;
        fileInfo->file = (char *) malloc(fileInfo->fileBytes);
        memset(fileInfo->file, '\0', fileInfo->fileBytes);
        memcpy(fileInfo->file, &message[1 + fileInfo->filenameBytes], fileInfo->fileBytes);
    }else if(operation == 'g'){
        fileInfo->fileBytes = getFileBytes(fileInfo->filename, serverInfo->directory);
        if(fileInfo->fileBytes > 0){
            fileInfo->file = (char *) malloc(fileInfo->fileBytes);
            memset(fileInfo->file, '\0', fileInfo->fileBytes);
        }else{
            fileInfo->file = NULL;
        }
    }else{
        fileInfo->fileBytes = 0;
        fileInfo->file = NULL;
    }
    
}
//Query the registry about a server - Returns NULL on error
RegistryResponse * askForServerInfo(RegistryInfo * regInfo, int server){
    char * regRequest;              //the request send to the registry
    int registrySocket;             //socket of communication with the registry
    RegistryResponse * response;      //the response to return

    //Ask the registry for the other server's details
    registrySocket = setupClientSocket(regInfo->registryIp, regInfo->registryPort);
    if(registrySocket < 0){
        printf("[Server %d][Error] Cannot connect to registry.\n", getpid());
        shutdown(registrySocket, SHUT_RDWR);
        close(registrySocket);
        return NULL;      
    }
    //Construct and Send Registry Message
    regRequest = (char *) malloc(MAX_REGISTRY_MESSAGE_SIZE);
    memset(regRequest, '\0', MAX_REGISTRY_MESSAGE_SIZE);
    regRequest[0] = 11;
    memcpy(&regRequest[1], &server, sizeof(int));
    transmitMessage(regRequest, MAX_REGISTRY_MESSAGE_SIZE, registrySocket, COM_ERR_MESSAGES);
    //Read Response From Registry
    memset(regRequest, '\0', MAX_REGISTRY_MESSAGE_SIZE);
    receiveMessage(regRequest, MAX_REGISTRY_MESSAGE_SIZE, registrySocket, COM_ERR_MESSAGES);
    if(regRequest[0] == 22){
        close(registrySocket);
        free(regRequest);
        return NULL; 
    }else if(regRequest[0] == 21){
        response = (RegistryResponse *) malloc(sizeof(RegistryResponse));
        memcpy(response->ip, &regRequest[1], INET_ADDRSTRLEN);
        memcpy(response->port, &regRequest[INET_ADDRSTRLEN+1], MAX_PORT_SIZE);
    }
    close(registrySocket);
    free(regRequest);
    return response;
}
//Sends a P_ERROR message to the client
void sendP_ERROR(int client_sock, char * message){
    message[0] = 24;
    message[1] = '\0';
    transmitMessage(message, 2, client_sock, COM_ERR_MESSAGES);
    shutdown(client_sock, SHUT_WR);
}
//This will handle PUT requests
void processPUT(RegistryInfo * regInfo, ServerInfo * serverInfo, int client_sock,  char * message, int messageBytes){
    int serverResponsible;              //the server returned from mapping a file. This one should handle the request
    int operationStatus;                //the status of success/failure returned by file operations
    unsigned long hashFileName;         //the hash of the file name
    unsigned long hashFile;             //the hash of the file's contents
    RegistryResponse * regResponse;     //will hold the response from a registry query
    FileInfo fileInfo;                  //information about the file
    int byteOffset;                     //will be used to calculate byte offsets inside message
    int redirectSocket;                 //used for communication with the server responsible for the request

    fillFileInformation('n', &fileInfo, serverInfo, message, messageBytes);
    serverResponsible = fileMapper(fileInfo.filename, regInfo->servers);
    if(serverResponsible == serverInfo->serverId){ //This one is the server responsible for handling the request
        fillFileInformation('p', &fileInfo, serverInfo, message, messageBytes);
        operationStatus = storeFile(fileInfo.filename, serverInfo->directory,  fileInfo.file, fileInfo.fileBytes);
        if(operationStatus == -1){  //error P_ERR 24
            sendP_ERROR(client_sock, message);
        }else{  //Success P_OK 23:hash(filename):hash(file)
            hashFileName = djb2hash((unsigned char *) fileInfo.filename, fileInfo.filenameBytes);
            hashFile = djb2hash((unsigned char *) fileInfo.file, fileInfo.fileBytes);
            message[0] = 23;
            byteOffset = 1;
            memcpy(&message[byteOffset], &hashFileName, sizeof(unsigned long));
            byteOffset += sizeof(unsigned long);
            memcpy(&message[byteOffset], &hashFile, sizeof(unsigned long));
            transmitMessage(message, MAX_SERVER_MESSAGE_SIZE, client_sock, COM_ERR_MESSAGES);
        }
    }else{ //Request needs to be forwarded to other server
        regResponse = askForServerInfo(regInfo, serverResponsible);
        if(regResponse == NULL){
            sendP_ERROR(client_sock, message);
        }else{ //redirect request to server responsible
            redirectSocket = setupClientSocket(regResponse->ip, regResponse->port);
            if(redirectSocket == -1){
                sendP_ERROR(client_sock, message);
            }else{
                transmitMessage(message, messageBytes, redirectSocket, COM_ERR_MESSAGES);
                shutdown(redirectSocket, SHUT_WR);
                messageBytes = receiveMessage(message, MAX_SERVER_MESSAGE_SIZE, redirectSocket, COM_ERR_MESSAGES);
                close(redirectSocket);
                transmitMessage(message, messageBytes, client_sock, COM_ERR_MESSAGES);
                shutdown(client_sock, SHUT_WR);   
            }
        }
    }
    //Release Resources
    free(fileInfo.filename);
    if(fileInfo.file != NULL){
        free(fileInfo.file);
    }
    close(client_sock);
}
//Sends a G_ERROR message to the client
void sendG_ERROR(int client_sock, char * message){
    message[0] = 27;
    message[1] = '\0';
    transmitMessage(message, 2, client_sock, COM_ERR_MESSAGES);
    shutdown(client_sock, SHUT_WR);
}
//This will handle GET requests
void processGET(RegistryInfo * regInfo, ServerInfo * serverInfo, int client_sock,  char * message, int messageBytes){
    int serverResponsible;              //the server returned from mapping a file. This one should handle the request
    int operationStatus;                //the status of success/failure returned by file operations
    unsigned long hashFileName;         //the hash of the file name
    unsigned long hashFile;             //the hash of the file's contents
    RegistryResponse * regResponse;     //will hold the response from a registry query
    FileInfo fileInfo;                  //information about the file
    int byteOffset;                     //will be used to calculate byte offsets inside message
    int totalMessageBytes;              //how many bytes will the message be

    fillFileInformation('n', &fileInfo, serverInfo, message, messageBytes);
    serverResponsible = fileMapper(fileInfo.filename, regInfo->servers);
    if(serverResponsible == serverInfo->serverId){ //This one is the server responsible for handling the request
        fillFileInformation('g', &fileInfo, serverInfo, message, messageBytes);
        operationStatus = readFile(fileInfo.filename, serverInfo->directory, fileInfo.file, fileInfo.fileBytes);
        if(operationStatus == -1){  //error G_ERR 27
            sendG_ERROR(client_sock, message);
        }else{  //G_OK 25:hash(filename):hash(file):FILE
            message[0] = 25;
            byteOffset = 1;
            hashFileName = djb2hash( (unsigned char *) fileInfo.filename, fileInfo.filenameBytes);
            hashFile = djb2hash( (unsigned char *) fileInfo.file, fileInfo.fileBytes);
            memcpy(&message[byteOffset], &hashFileName, sizeof(unsigned long));
            byteOffset += sizeof(unsigned long);
            memcpy(&message[byteOffset], &hashFile, sizeof(unsigned long));
            byteOffset += sizeof(unsigned long);
            memcpy(&message[byteOffset], fileInfo.file, fileInfo.fileBytes);
            totalMessageBytes = 1 + 2*(sizeof(unsigned long)) + fileInfo.fileBytes;
            transmitMessage(message, totalMessageBytes, client_sock, COM_ERR_MESSAGES);
            shutdown(client_sock, SHUT_WR);
        }
    }else{ //Request needs to be forwarded to other server
        regResponse = askForServerInfo(regInfo, serverResponsible);
        if(regResponse == NULL){
            sendG_ERROR(client_sock, message);
        }else{  //REDIRECT 26:IP:PORT
            message[0] = 26;
            byteOffset = 1;
            memcpy(&message[byteOffset], regResponse->ip, INET_ADDRSTRLEN);
            byteOffset += INET_ADDRSTRLEN;
            memcpy(&message[byteOffset], regResponse->port, MAX_PORT_SIZE);
            totalMessageBytes = 1 + INET6_ADDRSTRLEN + MAX_PORT_SIZE;
            transmitMessage(message, totalMessageBytes, client_sock, COM_ERR_MESSAGES);
            shutdown(client_sock, SHUT_WR);
        }
    }
    //Release Resources
    close(client_sock);
    free(fileInfo.filename);
    if(fileInfo.file != NULL){
        free(fileInfo.file);
    }
}
//Sends a D_ERROR message to the client
void sendD_ERROR(int client_sock, char * message){
    message[0] = 30;
    message[1] = '\0';
    transmitMessage(message, 2, client_sock, COM_ERR_MESSAGES);
    shutdown(client_sock, SHUT_WR);
}
//Sends a D_OK message to the client
void sendD_OK(int client_sock, char * message){
    message[0] = 29;
    message[1] = '\0';
    transmitMessage(message, 2, client_sock, COM_ERR_MESSAGES);
    shutdown(client_sock, SHUT_WR);
}
//This will handle DELETE requests
void processDELETE(RegistryInfo * regInfo, ServerInfo * serverInfo, int client_sock,  char * message, int messageBytes){
    int serverResponsible;              //the server returned from mapping a file. This one should handle the request
    int operationStatus;                //the status of success/failure returned by file operations
    RegistryResponse * regResponse;     //will hold the response from a registry query
    FileInfo fileInfo;                  //information about the file
    int redirectSocket;                 //used for communication with the server responsible for the request

    fillFileInformation('d', &fileInfo, serverInfo, message, messageBytes);
    serverResponsible = fileMapper(fileInfo.filename, regInfo->servers);
    if(serverResponsible == serverInfo->serverId){ //This one is the server responsible for handling the request
        memset(message, '\0', MAX_SERVER_MESSAGE_SIZE);
        operationStatus = deleteFile(fileInfo.filename, serverInfo->directory);
        if(operationStatus == -1){  //error D_ERR 30
            sendD_ERROR(client_sock, message);
        }else{  //D_OK 29
            sendD_OK(client_sock, message);
        }
    }else{ //Request needs to be forwarded to other server
        regResponse = askForServerInfo(regInfo, serverResponsible);
        if(regResponse == NULL){
            sendD_ERROR(client_sock, message);
        }else{ //Redirect message to server responsible
            redirectSocket = setupClientSocket(regResponse->ip, regResponse->port);
            if(redirectSocket == -1){
                sendD_ERROR(client_sock, message);
            }else{
                transmitMessage(message, messageBytes, redirectSocket, COM_ERR_MESSAGES);
                shutdown(redirectSocket, SHUT_WR);
                messageBytes = receiveMessage(message, MAX_SERVER_MESSAGE_SIZE, redirectSocket, COM_ERR_MESSAGES);
                close(redirectSocket);
                transmitMessage(message, messageBytes, client_sock, COM_ERR_MESSAGES);
                shutdown(client_sock, SHUT_WR);
            }
        }
    }
    //Release Resources
    free(fileInfo.filename);
    if(fileInfo.file != NULL){
        free(fileInfo.file);
    }
    close(client_sock);
}