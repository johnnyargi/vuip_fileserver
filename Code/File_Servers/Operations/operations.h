/*
This is an interface for the server to process PUT, GET and DELETE commands
*/
#include "../../Core_Modules/Communication_Protocol/communication.h"
//Structures ===============================================
//Structure to hold registry information
typedef struct registry_info_t{
    char * registryIp;      //The ip of the registry
    char * registryPort;    //The port of the registry
    int servers;            //The maximum number of servers, as retrieved by the registry
}RegistryInfo;
//Structure to hold sever information
typedef struct server_info_t{
    int serverId;           //The id of this server, as retrieved by the registry
    char * serverPort;      //This server's port
    char * directory;       //The directory to store the files
}ServerInfo;
//Structure to hold file information
typedef struct file_info_t{
    char * filename;        //the name of the file
    int filenameBytes;      //how many bytes is the filename
    char * file;            //the data of the file
    int fileBytes;          //how many bytes is the file data
}FileInfo;
//Structure to hold data returned from a query to the Registry
typedef struct registry_response_t{
    char ip[INET_ADDRSTRLEN];       //the ip in the registry response
    char port[MAX_PORT_SIZE];       //the port in the registry response
}RegistryResponse;
//Functionality ============================================
//This will handle PUT requests
void processPUT(RegistryInfo * regInfo, ServerInfo * serverInfo, int client_sock,  char * message, int messageBytes);
//This will handle GET requests
void processGET(RegistryInfo * regInfo, ServerInfo * serverInfo, int client_sock,  char * message, int messageBytes);
//This will handle DELETE requests
void processDELETE(RegistryInfo * regInfo, ServerInfo * serverInfo, int client_sock,  char * message, int messageBytes);