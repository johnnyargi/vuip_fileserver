//Implements the respective interface
#include "hasher.h"

//Hash contents of str. If you don't define the bytes, it could access memory beyond allocation
unsigned long djb2hash( unsigned char * str, int bytes){
    unsigned long hash = 5381;
    int c, i;

    for(i=0; i<bytes; i++){
        c = *str++;
        hash = ( ( hash << 5 ) + hash ) + c ; // hash ∗ 33 + c 
    }
    return hash ;
}