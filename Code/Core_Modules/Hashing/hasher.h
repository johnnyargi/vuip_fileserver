/*
Contains the hashing function used by server and client to verify 
data integrity
*/
//Hash contents of str. If you don't define the bytes, it could access memory beyond allocation
unsigned long djb2hash( unsigned char * str, int bytes);