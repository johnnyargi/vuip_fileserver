//This file will implement the respective .h interface
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netinet/in.h>
#include <netdb.h>
#include "client_setup.h"

//Functionality ==========================================
//Will setup a socket for a client connection. Returns -1 on error
int setupClientSocket(char * ip, char * port){
    int sock_fd;                            //socket file descriptor
    struct addrinfo hints;                  //specifications for getaddrinfo
    struct addrinfo *servinfo, *addrPtr;    //information retrieved by getaddrinfo
    int addrResult;                         //return value of getaddrinfo
    int connectionStatus;                   //if there was an error or success with connection

    //Retrieve information about the address
    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    addrResult = getaddrinfo(ip, port, &hints, &servinfo);
    if(addrResult != 0){
        fprintf(stderr, "[Client %d][Error] Get address info error : %s\n", getpid(), gai_strerror(addrResult));
    }
    //Connect to one of the given results
    for(addrPtr = servinfo; addrPtr != NULL; addrPtr = addrPtr->ai_next){
        sock_fd = socket(AF_INET, SOCK_STREAM, 0);
        if(sock_fd == -1){
            continue;
        }
        connectionStatus = connect(sock_fd, addrPtr->ai_addr, addrPtr->ai_addrlen);
        if(connectionStatus == -1){
            close(sock_fd);
            continue;
        }
        break;  //succesfull connection
    }
    if (addrPtr == NULL) { //exited loop without connection
        free(servinfo);
        return -1;
    }
    free(servinfo);
    return sock_fd;
}