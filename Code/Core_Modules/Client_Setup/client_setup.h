//This file will define an interface to create a client
#ifndef CLIENT_SETUP
#define CLIENT_SETUP

//Will setup a socket for a client connection. Returns -1 on error
int setupClientSocket(char * ip, char * port);
#endif