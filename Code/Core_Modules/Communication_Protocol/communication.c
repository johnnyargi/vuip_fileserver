/*
This file implements the interface of the respective .h file
*/
#include "communication.h"
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <errno.h>


//Transmit a message over the socket. Handle the case of not all bytes transmitted at once. Return -1 for failure/errors, otherwise 0. Print error messages on "displayErrorMessages" set to true.
int transmitMessage(char * message, int bytesToSend, int socket, bool displayErrorMessages){
    int bytesSent;          //how many bytes where sent after one call to receive
    int totalBytesSent;     //the sum of the bytes send so far
    bool error;

    bytesSent = 0;
    totalBytesSent = 0;
    error = false;
    while(totalBytesSent < bytesToSend){
        bytesSent = send(socket, &message[totalBytesSent], bytesToSend - totalBytesSent, 0);
        if(bytesSent <= 0){
            error = true;
            break;
        }
        totalBytesSent += bytesSent;
    }
    if(error){
        if(displayErrorMessages){
            if(bytesSent == 0){
                printf("Error in sending message : 0 bytes sent in last attempt. Total bytes to send : %d, but only %d where send so far\n", bytesToSend, totalBytesSent);
            }else{
                perror("Error in sending message ");
            }
        }
        return -1;
    }
    return 0;
}
//Receive a message from a socket. Handle the case of not all bytes received at once. Return -1 for failure/errors, otherwise bytes received. Print error messages on "displayErrorMessages" set to true.
int receiveMessage(char * bufferToFill, int maximumBytesExpected, int socket, bool displayErrorMessages){
    int bytesReceived;          //how many bytes where sent after one call to receive
    int totalBytesReceived;     //the sum of the bytes send so far
    bool error;

    bytesReceived = 0;
    totalBytesReceived = 0;
    error = false;
    while(totalBytesReceived < maximumBytesExpected){
        bytesReceived = recv(socket, &bufferToFill[totalBytesReceived], maximumBytesExpected - totalBytesReceived, 0);
        if(bytesReceived < 0){
            error = true;
            break;
        }else if(bytesReceived == 0){   //if the other side uses shutdown to denote end of writing, we can stop reading now
            break;
        }
        totalBytesReceived += bytesReceived;
    }
    if(totalBytesReceived == 0){
        if(displayErrorMessages){
            perror("Error : received 0 bytes totally.\n");
        }
    }
    if(error){
        if(displayErrorMessages){
            perror("Error in receiving message ");
        }
        return -1;
    }
    return totalBytesReceived;
}