/*
This file will be used to define and share constants related to the messaging protocol between client and server implementations.
It will also provide functions that transmit and receive messages, by handling special cases and errors, to avoid writing repeatable code.
*/
#ifndef COM_PROTOCOL
#define COM_PROTOCOL

#include <stdbool.h>

#define MAX_REGISTRY_MESSAGE_SIZE 30        //Maximum size of messages to/from the registry
#define MAX_SERVER_MESSAGE_SIZE 104857600   //Maximum size of messages to/from the server
#define MAX_PORT_SIZE 6                     //the maximum size of a string representing a port
#define COM_ERR_MESSAGES false              //use to easily switch the displayErrorMessages variable of all calls to the following functions

//Functionality ==============================================================
//Transmit a message over the socket. Handle the case of not all bytes transmitted at once. Return -1 for failure/errors, otherwise 0. Print error messages on "displayErrorMessages" set to true.
int transmitMessage(char * message, int bytesToSend, int socket, bool displayErrorMessages);
//Receive a message from a socket. Handle the case of not all bytes received at once. Return -1 for failure/errors, otherwise bytes received. Print error messages on "displayErrorMessages" set to true.
int receiveMessage(char * bufferToFill, int maximumBytesExpected, int socket, bool displayErrorMessages);
#endif