/*
This file is an interface for reading, writing and deleting files in the server
It will be used by operations.c to implement PUT, GET, DELETE operations
*/
#ifndef FILE_UTILS
#define FILE_UTILS

//Mapping function - Maps files to servers
int fileMapper(char *filename, int maximum_servers);
//Store a file to server's directory - Returns -1 if there's an error or 0 otherwise
int storeFile(char * fileName, char * directoryPath, char * fileData, int bytes);
//Read a file from server's directory and fill a buffer with its data - Returns -1 on error or the bytes read otherwise
int readFile(char * fileName, char * directoryPath, char * bufferToFill, int bytesToRead);
//Delete a file. Returns -1 on error
int deleteFile(char * fileName, char * directoryPath);
//Get the bytes of a file. Returns -1 on error
int getFileBytes(char * fileName, char * directoryPath);
#endif