/*
Implements respective interface
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../Hashing/hasher.h"
#include "file_utils.h"

//Mapping function - Maps files to servers
int fileMapper(char *filename, int maximum_servers){
    int sum, i, modulus;

    //just add the bytes of the filename and divide them by the number of servers to get the server
    for(i=0, sum=0; i<strlen(filename); i++){
        sum += filename[i];
    }
    modulus = sum % maximum_servers; 
    if(modulus == 0){
        return maximum_servers-1;
    }
    return modulus-1;
}
//Store a file to server's directory - Returns -1 if there's an error or 0 otherwise
int storeFile(char * fileName, char * directoryPath, char * fileData, int bytes){
    FILE * fptr;    //pointer to the file
    char * path;    //the full path of the file
    int offset;     //used to count bytes to contruct the path
    int i;          //counter

    if(directoryPath != NULL){  //store it in the given folder
        path = (char *) malloc(sizeof(char)*(strlen(fileName)+strlen(directoryPath)+2));
        strcpy(path, directoryPath);
        offset = strlen(directoryPath);
        path[offset] ='/';
        offset++;
    }else{  //store it locally
        offset = 0;
        path = (char *) malloc(sizeof(char)*(strlen(fileName)+1));
    }
    strcpy(&path[offset], fileName);
    fptr = fopen(path, "wb");
    if(fptr){
        for(i=0; i<bytes; i++){
            fwrite(&fileData[i], sizeof(char), 1, fptr);
        }
        fclose(fptr);
    }else{
        free(path);
        return -1;
    }
    free(path);
    return 0;
}
//Read a file from server's directory and fill a buffer with its data - Returns -1 on error or the bytes read otherwise
int readFile(char * fileName, char * directoryPath, char * bufferToFill, int bytesToRead){
    FILE * fptr;    //pointer to the file
    char * path;    //the full path of the file
    int offset;     //used to count bytes to contruct the path
    int bytesRead;  //counter

    if(directoryPath != NULL){
        path = (char *) malloc(sizeof(char)*(strlen(fileName)+strlen(directoryPath)+2));
        strcpy(path, directoryPath);
        offset = strlen(directoryPath);
        path[offset] = '/';
        offset++;
    }else{
        offset = 0;
        path = (char *) malloc(sizeof(char)*(strlen(fileName)+1));
    }
    strcpy(&path[offset], fileName);
    fptr = fopen(path, "rb");
    if(fptr){
        bytesRead=0;
        while(bytesRead < bytesToRead){
            fread(&bufferToFill[bytesRead], sizeof(char), 1, fptr);
            bytesRead++;
        }
        fclose(fptr);
    }else{
        free(path);
        return -1;
    }
    free(path);
    return bytesRead; //you read one extra byte for the end
}
//Delete a file. Returns -1 on error
int deleteFile(char * fileName, char * directoryPath){
    FILE * fptr;        //pointer to the file
    char * path;        //the full path of the file
    int offset;         //used to count bytes to contruct the path
    int removeResult;   //the result after the attempt to remove file

    if(directoryPath != NULL){  //delete it in the given folder
        path = (char *) malloc(sizeof(char)*(strlen(fileName)+strlen(directoryPath)+2));
        strcpy(path, directoryPath);
        offset = strlen(directoryPath);
        path[offset] ='/';
        offset++;
    }else{  //delete it locally
        offset = 0;
        path = (char *) malloc(sizeof(char)*(strlen(fileName)+1));
    }
    strcpy(&path[offset], fileName);
    fptr = fopen(path, "rb");
    if(fptr){
        removeResult = remove(path);
        fclose(fptr);
        if(removeResult == -1){
            free(path);
            return -1;
        }
    }else{
        free(path);
        return -1;
    }
    free(path);
    return 0; //you read one extra byte for the end
}
//Get the bytes of a file. Returns -1 on error
int getFileBytes(char * fileName, char * directoryPath){
    FILE * fptr;    //pointer to the file
    char * path;    //the full path of the file
    int offset;     //used to count bytes to contruct the path
    int fileBytes;  //counter

    if(directoryPath != NULL){
        path = (char *) malloc(sizeof(char)*(strlen(fileName)+strlen(directoryPath)+2));
        strcpy(path, directoryPath);
        offset = strlen(directoryPath);
        path[offset] = '/';
        offset++;
    }else{
        offset = 0;
        path = (char *) malloc(sizeof(char)*(strlen(fileName)+1) );
    }
    strcpy(&path[offset], fileName);
    fptr = fopen(path, "rb");
    if(fptr){
        fseek(fptr, 0L, SEEK_END);
        fileBytes = ftell(fptr);
        fclose(fptr);
        free(path);
        return fileBytes;
    }else{
        free(path);
        return -1;
    }
}