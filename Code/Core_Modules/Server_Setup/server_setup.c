//This file implements the interface of the respective .h file
#include <stdio.h>
#include <stdlib.h> 
#include <pthread.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include "server_setup.h"

//Globals =================================================
//Mutex for accept
pthread_mutex_t acceptMutex;
pthread_mutexattr_t acceptMutAttr;
//Thread variables
pthread_t workerId[MAX_POOL_SIZE];
pthread_attr_t workerAttr[MAX_POOL_SIZE];
//Other common vars
int sock_fd;            //server socket descriptor
struct sockaddr_in server_address;

//Functionality =========================================================================================
//This is used by threads to accept connections in a while loop. After accepting they run the function passed as argument
void * threadLoop(void * argument){
    int client_sock;        //the client socket descriptor
    struct sockaddr_in client_addr;
    socklen_t addrlen = sizeof(struct sockaddr_in);
    void (* requestProcessor) (int client_sock, char  * ipAddress) = argument;
    char ipAddress[INET_ADDRSTRLEN + 1];

    if(requestProcessor == NULL){
        printf("[Error] Invalid processing function passed to Server [%d]\n", getpid());
        exit(1);
    }
    while(1){
        //Accept connection
        pthread_mutex_lock(&acceptMutex);
        client_sock = accept(sock_fd, (struct sockaddr *) &client_addr, &addrlen);
        pthread_mutex_unlock(&acceptMutex);
        if(client_sock <0){
            continue;
        }
        //Store the ip address of this request - it will be used by requests to the registry
        inet_ntop(AF_INET, &client_addr.sin_addr.s_addr, ipAddress, INET_ADDRSTRLEN);
        //Process Request - Let this function decide what to do with the socket (close - keep alive)
        requestProcessor(client_sock, ipAddress);
    }
}
//Setup the multithreaded server to accept connections
void setupServer(int port, void (*requestProcessor)(int client_sock, char  * ipAddress)){
    int i;
    void * arg; //argument for thread function
    
    //Bind to address
    sock_fd = socket(AF_INET, SOCK_STREAM, 0);
    server_address.sin_family = AF_INET;
    server_address.sin_port = htons(port);
    server_address.sin_addr.s_addr = INADDR_ANY;
    if(bind(sock_fd, (struct sockaddr *) &server_address, sizeof server_address) == -1){
        printf("[Error] Server [%d] cannot bind\n", getpid());
        close(sock_fd);
        exit(1);
    }
    //Listen for connections 
    if(listen(sock_fd, BACK_LOG_SIZE) == -1){
        printf("[Error] Server [%d] cannot listen for connections\n", getpid());
        close(sock_fd);
        exit(1);
    }
    //Initialize mutex
    //Mutex creation to lock concurrent accept() calls
    pthread_mutexattr_init(&acceptMutAttr);
    pthread_mutex_init(&acceptMutex, &acceptMutAttr);
    //Initialize and spawn threads threads
    arg = requestProcessor;
    for(i=0; i<MAX_POOL_SIZE; i++){
        pthread_attr_init(&workerAttr[i]);
        pthread_create(&workerId[i], &workerAttr[i], threadLoop, arg);
        pthread_join(workerId[i], NULL);
    }
}
//Shutdown the server 
void shutdownServer(){
    shutdown(sock_fd, SHUT_RDWR);
    close(sock_fd);
    pthread_mutex_destroy(&acceptMutex);
}