#ifndef SERV_SETUP
#define SERV_SETUP
//This file will define an interface to spawn a multithreaded server
#include <netdb.h>

//Globals
#define MAX_POOL_SIZE 10
#define BACK_LOG_SIZE 5

//Functionality ================================================
//Set up the server to listen to the given port and pass a function for the threads to run
void setupServer(int port, void (*requestProcessor)(int client_sock, char * ipAddress));
//Shutdown the server 
void shutdownServer();
#endif